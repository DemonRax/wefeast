import React from 'react';
import { Image } from 'react-bootstrap';
import { handleLogin } from './Firebase';

export const Login: React.FC = () => {
    const onLogin = () => handleLogin('Google');

    return (
        <div>
            <Image src="/img/google.png" className="sign-in" onClick={onLogin} />
        </div>
    );
};
