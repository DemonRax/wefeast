import { useCallback, useState } from 'react';
import { Recipe } from './Recipe';
import { addRecipe, deleteRecipe, useFetchRecipes, updateRecipe } from './Firebase';
import * as admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

const randomDish = (recipes: DocumentData[]): number => Math.floor(Math.random() * recipes.length);

export const useFeast = () => {
    const [selected, setSelected] = useState<string | undefined>(undefined);
    const [recipes, setRecipes] = useState<DocumentData[]>([]);
    const [open, setOpen] = useState(false);
    const [edit, setEdit] = useState<DocumentData | null>(null);

    useFetchRecipes(setRecipes);

    const pickRandom = useCallback(() => {
        setSelected(randomDish(recipes).toString());
    }, [recipes]);

    const switchOpen = useCallback(() => {
        setOpen(!open);
    }, [open]);

    const openCard = useCallback(
        (i: string | undefined) => () => {
            if (selected === i) {
                i = undefined;
            }
            setSelected(i);
        },
        [selected]
    );

    const onDelete = useCallback(
        (id: string) => () => {
            deleteRecipe(id);
        },
        []
    );

    const onSave = useCallback(
        (r: Recipe) => {
            setOpen(false);
            if (edit) {
                updateRecipe(edit.id, r);
            } else {
                addRecipe(r);
            }
        },
        [edit]
    );

    const onEdit = useCallback(
        (d: DocumentData) => () => {
            setOpen(true);
            setEdit(d);
        },
        []
    );

    return { selected, recipes, open, edit, pickRandom, switchOpen, openCard, onDelete, onSave, onEdit };
};
