import React, { useEffect } from 'react';
import { Badge } from 'react-bootstrap';
import * as admin from 'firebase';
import { useIngredients } from './IngredientsHooks';

type DocumentData = admin.firestore.DocumentData;

type IngredientsProps = {
    initial: string[];
    onChange: (i: string[]) => void;
};

export const Ingredients: React.FC<IngredientsProps> = ({ initial, onChange }) => {
    const { selected, ingredients, onSelect } = useIngredients(initial);

    useEffect(() => {
        onChange(selected);
    }, [selected, onChange]);

    return (
        <div>
            {ingredients.map((d: DocumentData) => (
                <Badge
                    pill
                    variant={selected.includes(d.name) ? 'dark' : 'light'}
                    key={d.id}
                    onClick={onSelect(d.name)}>
                    {d.name}
                </Badge>
            ))}
        </div>
    );
};
