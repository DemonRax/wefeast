import React from 'react';
import { Badge } from 'react-bootstrap';

type IngredientsViewProps = {
    ingredients: string[];
};

export const IngredientsView: React.FC<IngredientsViewProps> = ({ ingredients }) => {
    if (ingredients === undefined || ingredients.length === 0) {
        return <div>Индгридиенты не указаны</div>;
    }

    return (
        <div>
            {ingredients.sort().map((i) => (
                <Badge pill variant="dark" key={i}>
                    {i}
                </Badge>
            ))}
        </div>
    );
};
