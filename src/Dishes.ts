export const DISHES = Array.from([
    {
        Title: 'Spaghetti Bolognese',
        Time: 60,
        Ingredients: [
            'Onions',
            'Ground beef',
            'Pasta',
            'Garlic',
            'Tomatoes sliced',
            'Basil',
            'Carrots',
            'Celery',
            'Cheese'
        ]
    },
    {
        Title: 'Mega Meatballs',
        Time: 90,
        Ingredients: ['Onions', 'Ground beef', 'Garlic', 'Tomato sauce', 'Basil', 'Carrots', 'Chickpea']
    },
    {
        Title: 'Super Sausages',
        Time: 90,
        Ingredients: [
            'Onions',
            'Sausages',
            'Garlic',
            'Basil',
            'Thyme',
            'Carrots',
            'Balsamico',
            'Parsley',
            'Beef stock',
            'Champignons'
        ]
    }
]);
