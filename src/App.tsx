import React from 'react';
import { Feast } from './Feast';
import { useAuth } from './AuthHooks';
import { Login } from './Login';
import './App.scss';

export const App: React.FC = () => {
    const { authenticated } = useAuth();

    if (authenticated) {
        return <Feast />;
    }
    return <Login />;
};
