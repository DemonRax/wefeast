import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import { AuthProvider } from './AuthProvider';

ReactDOM.render(
    // <React.StrictMode>
    <Suspense fallback={<div>Loading...</div>}>
        <AuthProvider>
            <App />
        </AuthProvider>
    </Suspense>,
    document.getElementById('root')
);
