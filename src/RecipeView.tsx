import React from 'react';
import { Accordion, Card } from 'react-bootstrap';
import { IngredientsView } from './IngredientsView';
import { RecipeTitle } from './RecipeTitle';

type RecipeProps = {
    title: string;
    time: number;
    ingredients: string[];
    eventKey: string;
    onClick: () => void;
    onDelete: () => void;
    onEdit: () => void;
};

export const RecipeView: React.FC<RecipeProps> = ({
    title,
    time,
    ingredients,
    eventKey,
    onClick,
    onDelete,
    onEdit
}) => (
    <Card key={eventKey}>
        {/* <Accordion.Toggle as={Card.Header} eventKey={eventKey}>
            <RecipeTitle title={title} time={time} onDelete={onDelete} onClick={onClick} />
        </Accordion.Toggle> */}
        <Card.Header>
            <RecipeTitle title={title} time={time} onEdit={onEdit} onDelete={onDelete} onClick={onClick} />
        </Card.Header>
        <Accordion.Collapse eventKey={eventKey}>
            <Card.Body>
                <IngredientsView ingredients={ingredients} />
            </Card.Body>
        </Accordion.Collapse>
    </Card>
);
