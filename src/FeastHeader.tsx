import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BiRefresh, BiDish, BiLogOutCircle } from 'react-icons/bi';
import { handleLogout } from './Firebase';

type FeastHeaderProps = {
    pickRandom: () => void;
    switchOpen: () => void;
};

export const FeastHeader: React.FC<FeastHeaderProps> = ({ pickRandom, switchOpen }) => {
    const onLogout = () => handleLogout();

    return (
        <Container>
            <Row>
                <Col xs={10}>
                    <span className="btn-large" title="Случайное блюдо">
                        <BiRefresh onClick={pickRandom} />
                    </span>
                    <span className="btn-large" title="Добавить блюдо">
                        <BiDish onClick={switchOpen} />
                    </span>
                </Col>
                <Col xs={2}>
                    <span className="btn-large">
                        <BiLogOutCircle className="logout" onClick={onLogout} title="Выйти" />
                    </span>
                </Col>
            </Row>
        </Container>
    );
};
