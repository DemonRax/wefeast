import { useEffect } from 'react';
import { Recipe } from './Recipe';
import { app } from './Base';
import * as admin from 'firebase';
import firebase from 'firebase';

type DocumentData = admin.firestore.DocumentData;
const dbRecipes = app.firestore().collection('recipes');
const dbIngredients = app.firestore().collection('ingredients');

export const handleLogin = (sns: any) => {
    let provider: firebase.auth.AuthProvider;
    switch (sns) {
        case 'Google':
            provider = new firebase.auth.GoogleAuthProvider();
            break;

        default:
            throw new Error('Unsupported SNS' + sns);
    }

    firebase.auth().signInWithPopup(provider);
    // TODO handle errors
    // .catch((e) => console.log(e));
};

export const handleLogout = () => {
    // event.preventDefault();
    firebase.auth().signOut();
};

export const useFetchRecipes = (setRecipes: (d: DocumentData[]) => void): void => {
    useEffect(
        () =>
            dbRecipes.orderBy('title').onSnapshot((snapshot) => {
                const temp: DocumentData[] = [];
                snapshot.forEach((doc) => {
                    temp.push({ ...doc.data(), id: doc.id });
                });
                setRecipes(temp);
            }),
        [setRecipes]
    );
};

export const addRecipe = (recipe: Recipe): string | null => {
    if (!recipe.title || recipe.time === 0) {
        return 'error';
    }
    dbRecipes.add(recipe);
    return null;
};

export const updateRecipe = (id: string, recipe: Recipe): string | null => {
    if (!recipe.title || recipe.time === 0) {
        return 'error';
    }
    const record = dbRecipes.doc(id);
    if (record) {
        record.update(recipe);
    } else {
        addRecipe(recipe);
    }
    return null;
};

export const deleteRecipe = (id: string): void => {
    dbRecipes.doc(id).delete();
};

export const useFetchIngredients = (setIngredients: (d: DocumentData[]) => void): void => {
    useEffect(
        () =>
            dbIngredients.orderBy('name').onSnapshot((snapshot) => {
                const temp: DocumentData[] = [];
                snapshot.forEach((doc) => {
                    temp.push({ ...doc.data(), id: doc.id });
                });
                setIngredients(temp);
            }),
        [setIngredients]
    );
};

export const addIngredient = (ingredient: string) => {
    if (ingredient) {
        dbIngredients.add({ name: ingredient });
    }
};
