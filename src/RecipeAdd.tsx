import React from 'react';
import { Accordion, Card, Form, Row, Col, InputGroup } from 'react-bootstrap';
import { BiMessageSquareAdd, BiMessageSquareCheck, BiMessageSquareX } from 'react-icons/bi';
import { Recipe } from './Recipe';
import { Ingredients } from './Ingredients';
import * as admin from 'firebase';
import { useRecipeAdd } from './RecipeAddHooks';

type DocumentData = admin.firestore.DocumentData;

type RecipeAddProps = {
    open: boolean;
    toEdit: DocumentData | null;
    onSave: (r: Recipe) => void;
};

export const RecipeAdd: React.FC<RecipeAddProps> = ({ open, toEdit, onSave }) => {
    const {
        title,
        time,
        ingredients,
        ingredient,
        eventKey,
        reset,
        onSubmit,
        onTitleChange,
        onTimeChange,
        onChangeIngredients,
        onChangeIngredient,
        newIngredient
    } = useRecipeAdd(open, toEdit, onSave);

    return (
        <Accordion activeKey={eventKey}>
            <Card key="open" className="card-no-border">
                <Accordion.Collapse eventKey="open">
                    <Card.Body>
                        <Form>
                            <Row>
                                <Col md={8} xs={6}>
                                    <Form.Control placeholder="Название блюда" value={title} onChange={onTitleChange} />
                                </Col>
                                <Col md={2} xs={3}>
                                    <Form.Control placeholder="Время, мин" value={time} onChange={onTimeChange} />
                                </Col>
                                <Col md={2} xs={3}>
                                    <BiMessageSquareCheck
                                        onClick={onSubmit}
                                        className="btn-large"
                                        title="Новое блюдо"
                                    />
                                    <BiMessageSquareX onClick={reset} className="btn-large" title="Отменить" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} xs={3}>
                                    <InputGroup>
                                        <Form.Control
                                            placeholder="Новый"
                                            value={ingredient}
                                            onChange={onChangeIngredient}
                                            className="new-ingredient"
                                        />
                                        <InputGroup.Append>
                                            <InputGroup.Text>
                                                <BiMessageSquareAdd
                                                    onClick={newIngredient}
                                                    className="btn-medium"
                                                    title="Добавить"
                                                />
                                            </InputGroup.Text>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Col>
                                <Col md={9} xs={8}>
                                    <Ingredients initial={ingredients} onChange={onChangeIngredients} />
                                </Col>
                            </Row>
                        </Form>
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
        </Accordion>
    );
};
