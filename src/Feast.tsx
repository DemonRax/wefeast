import React from 'react';
import { Accordion } from 'react-bootstrap';
import { RecipeView } from './RecipeView';
import { RecipeAdd } from './RecipeAdd';
import { FeastHeader } from './FeastHeader';
import { useFeast } from './FeastHooks';
import * as admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

export const Feast: React.FC = () => {
    const { selected, recipes, open, edit, pickRandom, switchOpen, openCard, onDelete, onSave, onEdit } = useFeast();

    return (
        <div className="App" >
            <FeastHeader pickRandom={pickRandom} switchOpen={switchOpen} />
            <RecipeAdd open={open} onSave={onSave} toEdit={edit} />
            <Accordion activeKey={selected}>
                {recipes.map((d: DocumentData, i: number) => (
                    <RecipeView
                        title={d.title}
                        time={d.time}
                        ingredients={d.ingredients}
                        eventKey={i.toString()}
                        key={i}
                        onClick={openCard(i.toString())}
                        onDelete={onDelete(d.id)}
                        onEdit={onEdit(d)}
                    />
                ))}
            </Accordion>
        </div>
    );
};
