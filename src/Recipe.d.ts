export type Recipe = {
    title: string;
    time: number;
    ingredients: string[];
};
