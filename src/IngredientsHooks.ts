import { useCallback, useEffect, useState } from 'react';
import { useFetchIngredients } from './Firebase';
import * as admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

export const useIngredients = (initial: string[]) => {
    const [ingredients, setIngredients] = useState<DocumentData[]>([]);
    const [selected, setSelected] = useState<string[]>(initial);

    useFetchIngredients(setIngredients);

    useEffect(() => {
        setSelected(initial);
    }, [initial]);

    const onSelect = useCallback(
        (id: string) => () => {
            let found = false;
            const newSelected = selected.filter((v) => {
                if (v === id) {
                    found = true;
                    return false;
                }
                return true;
            });
            if (!found) {
                newSelected.push(id);
            }
            setSelected(newSelected);
        },
        [selected]
    );

    return { selected, ingredients, onSelect };
};
