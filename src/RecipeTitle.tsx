import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BiMessageSquareEdit, BiMessageSquareX } from 'react-icons/bi';

type RecipeTitleProps = {
    title: string;
    time: number;
    onClick: () => void;
    onDelete: () => void;
    onEdit: () => void;
};

export const RecipeTitle: React.FC<RecipeTitleProps> = ({ title, time, onClick, onDelete, onEdit }) => (
    <Container>
        <Row>
            <Col md={8} xs={6} onClick={onClick}>
                {title}
            </Col>
            <Col md={2} xs={3} onClick={onClick}>
                {time} минут
            </Col>
            <Col md={2} xs={3}>
                <BiMessageSquareEdit onClick={onEdit} className="btn-medium" />
                <BiMessageSquareX onClick={onDelete} className="btn-medium" />
            </Col>
        </Row>
    </Container>
);
