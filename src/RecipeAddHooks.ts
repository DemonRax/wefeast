import { ChangeEvent, useCallback, useState, useEffect } from 'react';
import { Recipe } from './Recipe';
import { addIngredient } from './Firebase';
import * as admin from 'firebase';

type DocumentData = admin.firestore.DocumentData;

export const useRecipeAdd = (open: boolean, toEdit: DocumentData | null, onSave: (r: Recipe) => void) => {
    const [title, setTitle] = useState('');
    const [time, setTime] = useState(0);
    const [ingredients, setIngredients] = useState<string[]>([]);
    const [ingredient, setIngredient] = useState('');
    const [eventKey, setEventKey] = useState('');

    useEffect(() => {
        setEventKey(open ? 'open' : '');
    }, [open]);

    const reset = useCallback(() => {
        setTitle('');
        setTime(0);
        setIngredients([]);
        onSave({ title: title, time: time, ingredients: ingredients });
    }, [title, time, ingredients, onSave]);

    const onSubmit = useCallback(() => {
        onSave({ title: title, time: time, ingredients: ingredients });
        setTitle('');
        setTime(0);
        setIngredients([]);
    }, [onSave, time, title, ingredients]);

    const onTitleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        setTitle(e.target.value);
    }, []);

    const onTimeChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        setTime(Number(e.target.value));
    }, []);

    const onChangeIngredients = useCallback((list: string[]) => {
        setIngredients(list);
    }, []);

    const onChangeIngredient = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        setIngredient(e.target.value);
    }, []);

    const newIngredient = useCallback(() => {
        addIngredient(ingredient);
        setIngredient('');
    }, [ingredient, setIngredient]);

    useEffect(() => {
        if (toEdit) {
            setEventKey('open');
            setTitle(toEdit.title);
            setTime(toEdit.time);
            setIngredients(toEdit.ingredients);
        }
    }, [toEdit]);

    return {
        title,
        time,
        ingredients,
        ingredient,
        eventKey,
        reset,
        onSubmit,
        onTitleChange,
        onTimeChange,
        onChangeIngredients,
        onChangeIngredient,
        newIngredient
    };
};
